import React, { useState } from "react";
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	Modal,
	Keyboard,
} from "react-native";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import MainButtons from "./MainButtons";
import Card from "./Card";

const Goaloutput = (props) => {
	const [currentValue, setCurrentValue] = useState(props.title);
	const [isVisible, setIsVisible] = useState(false);
	const [valueEdited, setValueEdited] = useState(currentValue);

	const editInputHandler = (currentEditGoal) => {
		setCurrentValue(currentEditGoal);
		setIsVisible(true);
		Keyboard.dismiss();
	};

	const editText = (editedText) => {
		setValueEdited(editedText);
	};

	const editSaver = () => {
		setIsVisible(false);
		setCurrentValue(valueEdited);
	};

	return (
		<View>
			<View style={styles.buttonContainer}>
				<View style={styles.listItem}>
					<Text style={styles.listContent}>{currentValue}</Text>
					<Text style={styles.listContent}>{props.index}</Text>
				</View>

				<View style={styles.editdeleteConainer}>
					<MainButtons onPress={props.onDelete.bind(this, props.id)}>
						<Ionicons name="trash-sharp" size={40} />
					</MainButtons>
					<MainButtons
						onPress={editInputHandler.bind(this, props.title)}
						style={styles.buttonEdit}
					>
						<AntDesign name="edit" size={40} />
					</MainButtons>
				</View>
			</View>

			<Modal visible={isVisible}>
				<View style={styles.modalCard}>
					<Card style={styles.insideCard}>
						<View>
							<MainButtons
								onPress={() => setIsVisible(false)}
								style={styles.backButton}
							>
								<Ionicons
									name="arrow-back-circle-outline"
									size={50}
								/>
							</MainButtons>
						</View>
						<TextInput
							placeholder="edit Here"
							style={styles.editlist}
							onChangeText={editText}
							value={valueEdited}
						/>
						<MainButtons onPress={editSaver}>
							<Ionicons name="checkbox-sharp" size={50} />
						</MainButtons>
					</Card>
				</View>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	listItem: {
		borderColor: "black",
		borderWidth: 3,
		borderRadius: 9,
		padding: 10,
		marginTop: 10,
		width: "75%",
		alignItems: "center",
		justifyContent: "space-between",
		elevation: 10,
		backgroundColor: "black",
		flexDirection: "row",
	},
	buttonContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	buttonEdit: {
		paddingLeft: 5,
	},
	listContent: {
		fontSize: 19,
		color: "white",
		fontWeight: "bold",
	},
	modalCard: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "black",
	},
	editlist: {
		borderBottomColor: "black",
		borderBottomWidth: 3,
		width: "80%",
		fontSize: 19,
		fontWeight: "bold",
	},
	insideCard: {
		flexDirection: "row",
		justifyContent: "space-between",
		width: "95%",
		alignItems: "center",
	},
	editdeleteConainer: {
		flexDirection: "row",
		paddingTop: 20,
		justifyContent: "space-between",
	},
});

export default Goaloutput;
