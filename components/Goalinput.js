import React, { useState } from "react";
import { View, TextInput, StyleSheet } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import MainButtons from "./MainButtons";

const Goalinput = (props) => {
	const [enteredGoal, setEnteredGoal] = useState("");

	const inputGoalHandler = (enteredText) => {
		setEnteredGoal(enteredText);
	};

	const addGoalHandler = () => {
		props.onAddGoal(enteredGoal);
		setEnteredGoal("");
	};

	let currentValue = enteredGoal;

	return (
		<View style={styles.inputContainer}>
			<TextInput
				placeholder="Add Tasks"
				style={styles.input}
				onChangeText={inputGoalHandler}
				value={currentValue}
			/>
			<MainButtons style={styles.buttonAdd} onPress={addGoalHandler}>
				<Ionicons name="add-circle" size={55} />
			</MainButtons>
		</View>
	);
};

const styles = StyleSheet.create({
	inputContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
	},
	input: {
		width: "80%",
		borderBottomColor: "black",
		borderBottomWidth: 3,
		fontSize: 19,
		elevation: 9,
		backgroundColor: "white",
		padding: 10,
		borderRadius: 50,
		fontWeight: "bold",
	},
	buttonAdd: {
		paddingLeft: 15,
	},
});

export default Goalinput;
