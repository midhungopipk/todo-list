import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";

const MainButtons = (props) => {
	return (
		<TouchableOpacity activeOpacity={1} onPress={props.onPress}>
			<View style={{ ...styles.button, ...props.style }}>
				<Text style={{ ...styles.buttonTitle, ...props.style }}>
					{props.children}
				</Text>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	buttonTitle: {
		color: "black",
		fontWeight: "bold",
	},
});

export default MainButtons;
