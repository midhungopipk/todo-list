import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Header = (props) => {
	return (
		<View style={styles.header}>
			<Text style={styles.headerTitle}> ToDo List</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	header: {
		width: "100%",
		height: 100,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "grey",
		paddingTop: 40,
	},
	headerTitle: {
		color: "white",
		fontSize: 29,
		fontWeight: "bold",
	},
});

export default Header;
