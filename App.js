import React, { useState } from "react";
import { StyleSheet, View, FlatList, SafeAreaView } from "react-native";
import Header from "./components/Header";
import Goaloutput from "./components/Goaloutput";
import Goalinput from "./components/Goalinput";

export default function App() {
	const [courseGoals, setCourseGoals] = useState([]);

	const addGoalHandler = (goalTitle) => {
		if (goalTitle === "") {
			return;
		}
		setCourseGoals((courseGoals) => [
			...courseGoals,
			{ id: Math.random().toString(), value: goalTitle },
		]);
	};

	const goalRemoveHandler = (goalid) => {
		setCourseGoals((courseGoals) => {
			return courseGoals.filter((goal) => goal.id != goalid);
		});
	};

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<Header />
			<View style={styles.screen}>
				<Goalinput onAddGoal={addGoalHandler} />
				<View>
					<FlatList
						contentContainerStyle={{ paddingBottom: 20 }}
						keyExtractor={(item, index) => item.id}
						data={courseGoals}
						renderItem={({ item, index }) => (
							<Goaloutput
								index={index + 1}
								id={item.id}
								title={item.value}
								onDelete={goalRemoveHandler}
							/>
						)}
					/>
				</View>
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		paddingTop: 30,
	},
});
